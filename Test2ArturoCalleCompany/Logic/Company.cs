﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Logic
{
    public class Company
    {

        private double price;
        private double discount;
        private double subtotal;
        private double pricetotal;
        private int numCostume;//NUMERO DE PRENDAS
        private double total;
        private double discount1, discount2;

        public double Price { get => price; set => price = value; }
        public double Discount { get => discount; set => discount = value; }
        public double Subtotal { get => subtotal; set => subtotal = value; }
        public double Pricetotal { get => pricetotal; set => pricetotal = value; }
        public int NumCostume { get => numCostume; set => numCostume = value; }
        public double Total { get => total; set => total = value; }
        public double Discount1 { get => discount1; set => discount1 = value; }
        public double Discount2 { get => discount2; set => discount2 = value; }

        public String newbuy(double price, String articulo)
        {
            double costo;
            String Msj;
            if (price > 125000)
            {
                Msj = "Articulo: " + articulo + " Subtotal: " + price + " Descuento:35%" + "<br>Total a pagar es: " + Convert.ToString(price- (price * 0.35));               /
            }
            else
            {
                Msj = "Articulo: " + articulo + " Subtotal: " + price + "  Descuento:10%" + "Total a pagar es: " + Convert.ToString(price - (price * 0.10));

            }
            return Msj;

        }

    }

}

    }
}